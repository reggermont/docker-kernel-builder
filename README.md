# Kernel Builder

![](assets/install-success.png)

Kernel builder for your own distrib (builder importing your current kernel config as a base for the new kernel or taking a config that you import)  
Supports all versions starting from 3.0.0 (as 1.x and 2.x are not available officially anymore) and aptitude-based distrib (As it generates only *.deb files, so Debian, Ubuntu, ...)

## Why using this solution ?

* Compile your kernel yourself without any knowledge
* Always have you kernel up-to-date
* Completely dockerized (you only need docker, docker-compose, make and bash support)
* Kernel built with latest gcc version
* Terminal outputs saved in file in `./logs/` folder with date append.

## How to build

* Be sure that the user running the script is allowed to run docker and docker-compose commands
* Copy `.env.dist` to `env` and change values according to your needs
* Run 
    * `make` to build latest stable version [(that you can retrieve here)](https://www.kernel.org/)
    * `make version=x.x` to build latest patch version of a minor version (example: `make version=4.8` will build version 4.8.17 of linux kernel)
    * `make version=x.x.x` to build a specific version.

* When make command is over, you can find your built kernel in `./output/` folder

## How to install

Like any .deb package. As root, run `sudo dpkg -i ./output/*.deb` and just restart your computer to boot your new kernel.

If your kernel doesn't boot, I'm sorry for that. Just select your previous kernel at boot and uninstall with `sudo apt remove ...`

## To do list

See [TODO.md](docs/TODO.md)

## License

This project is under [Apache 2.0](LICENSE) License
