# Changelog

[Return to README](README.md)

## Summary
* [1.2.0 (2020-02-06)](#120-2020-02-06) 
* [1.1.0 (2018-12-16)](#110-2018-12-16)
* [1.0.0 (2018-09-17)](#100-2018-09-17)

[Return to summary](#summary)
## 1.2.0 (2020-02-06) 

### Added
- Allow support for building all latest minor versions starting with 3.0 (as 1.x and 2.x are not available officially anymore) 
- New gitmodule for eol versions: [kernel-eol-versions](https://gitlab.com/reggermont/kernel-eol-versions).
- Entry verification (for kernel version), preventing unexpected results
- Code documentation

### Changed
- Reorganized bash files (`build.sh` --> `kernelbuilder.sh`, `versionchecker.sh` --> `versionmanager.sh`, new `common.sh`) and functions, to be more understandable
- Makefile pre-build reorganization, 
- README.md, according to changes made on the project
- Dockerfile: From `MAINTAINER` to `LABEL maintainer` (deprecated by docker team)
- License, now under Apache 2.0

### Fixed
- Missing carriage returns

[Return to summary](#summary)
## 1.1.0 (2018-12-16) 

### Added
* Support for building latest stable kernel (just run `make` and script will get latest version number)
* New file : scripts/versionchecker.sh : scripts library managing version (get latest version and experimental code to be used later)
* Support for custom .config (just copy file at project root and name it `.config`)
* Support for number of cores to use for kernel build (in `.env`)
* Support for custom architecture to build the kernel (in `.env`)

### Changed
* Completely reworked `build.sh` | `Makefile` | `entrypoint.sh` to be more readable and more logic for user and dev (still need work tbh)
* All scripts are now in `scripts/` folder
* Name of compiled files (more proper than before)
* Add .env in docker-compose.yml's `env_file`
* Updated `README.md`

### Fixed
* Useless code in `Dockerfile`

[Return to summary](#summary)
## 1.0.0 (2018-09-17)

### Added
* Initial release

[Return to summary](#summary)
