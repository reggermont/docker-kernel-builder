#!/bin/bash

FILEPATH=$(dirname $(readlink -f $0))

. ${FILEPATH}/common.sh
. ${FILEPATH}/kernel-eol-versions/kernel-eol-versions.sh

LINK=https://www.kernel.org/releases.json

## @var     $1  String to check
##
## @echo        1 if string matches a kernel version, 0 otherwise
is_a_kernel_version() {

    if [[ $1 =~ ^[3-9]\.[0-9]{1,2}\.[0-9]{1,3}$ ]]; then
        echo 1
    else
        echo 0
    fi

}

## @var     $1  String to check
##
## @echo        1 if string matches a kernel version without the patch version, 0 otherwise
is_a_minor_kernel_version() {
  
    if [[ $1 =~ ^[3-9]\.[0-9]{1,2}$ ]]; then
        echo 1
    else
        echo 0
    fi

}

## @return      current kernel version
get_current_kernel_version() {

    current="`uname -r`"
    current=${current%%-*}
    trim_spaces $current; RETURN=${RETURN}

}

## @return      latest stable kernel version
get_latest_stable_kernel_version() {

    latest="`wget -qO- ${LINK}`"
    latest=${latest%%\}*}
    latest=${latest##*version\":}
    latest=${latest//\"}
    trim_spaces $latest; RETURN=${RETURN}

}

## @var     $1  kernel version without patch version
##
## @return      latest kernel version according to minor version provided
get_latest_minor_kernel_version() {

    major_version=${1:0:1}
    minor_version=${1:2:4}

    var_name="KERNEL_V${major_version}_${minor_version}" 
    
    if [ -z ${!var_name} ]; then
        patch_version="`wget -qO- ${LINK}`"
        patch_version=${patch_version##*\"version\":\ \"${1}\.}
        patch_version=${patch_version%%\"*}
        if [[ ${patch_version} =~ ^[0-9]{1,3}$ ]]; then
            full_version="${major_version}.${minor_version}.${patch_version}"
        else
            echo "Minor version not found. Aborting ..."
            exit 1
        fi
    else
        full_version=${!var_name}
    fi

    trim_spaces $full_version; RETURN=${RETURN}

}

## @var     $1  string matching a kernel version to build
##
## @return      latest version available, according to string given
get_kernel_version_to_build() {

    get_current_kernel_version; current_version=${RETURN}

    if [ -z $1 ]; then
        get_latest_stable_kernel_version; new_version=${RETURN}
    elif [ $(is_a_kernel_version $1) -eq 1 ]; then
        new_version=$1
    elif [[ $(is_a_minor_kernel_version $1) -eq 1 ]]; then
        get_latest_minor_kernel_version $1; new_version=${RETURN}
    else
        echo "Version given don't match a valid kernel version. Aborting ..."
        exit 1;
    fi


    if [ "${current_version}" == "${new_version}" ]; then
        echo "Latest already installed. Aborting ..."
        exit 1;
    else
        echo "New version is available. (Current is ${current_version}, new one is ${new_version})"
    fi

    RETURN=${new_version}

}
