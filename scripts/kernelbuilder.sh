#!/bin/bash

FILEPATH=$(dirname $(readlink -f $0))

. ${FILEPATH}/../.env
. ${FILEPATH}/versionmanager.sh
. ${FILEPATH}/common.sh

NOW=$(date +'%Y%m%d-%H%M%S')

## @var $1 Kernel version to build given by the user
prepare() {

    get_kernel_version_to_build $1; version=${RETURN}

    echo "KERNEL_VERSION=${version}" > ${FILEPATH}/../.version
    echo "KERNEL_MAJOR_VERSION=${version:0:1}" >> ${FILEPATH}/../.version   

    cd ${FILEPATH}/../ && make prepare

	echo "Building version $1 of the kernel on ${NUMBER_CORES_USE} core(s)"
    
}

timed_build() {

    start_timestamp=`date +%s`

    cd ${FILEPATH}/../ && make build

    end_timestamp=`date +%s`

    time=$(( $end_timestamp - $start_timestamp ))
    hours=$(( $time/3600 ))
    time2=$(( $time-3600*$hours ))
    minutes=$(( $time2/60 ))
    seconds=$(( $time2-60*$minutes   ))    

    echo "Total script time: ${start_message} ${hours}h ${minutes}m ${seconds}s ${end_message} on ${NUMBER_CORES_USE} core(s)" 

}

clean() {

    rm ${FILEPATH}/../.version
	rm ${FILEPATH}/../.config

}

prepare $1
timed_build |& tee ${FILEPATH}/../logs/log-${NOW}.txt
clean
